#include "settingswidget.h"

#include <QTabWidget>
#include <QBoxLayout>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QValidator>
#include <QSettings>

SettingsWidget::SettingsWidget(QWidget *parent) : QWidget(parent)
{
    setWindowTitle("Settings");
    readSettings();
    createWidgets();
}

SettingsWidget::~SettingsWidget()
{
    writeSettings();
    if (lineEditPortPbx->text().toInt() != portPbx)
        emit signalWriteSettingsPbx();
    else if (lineEditHost->text()     != hostName ||
             lineEditBase->text()     != baseName ||
             lineEditUser->text()     != userName ||
             lineEditPassword->text() != password) {
        emit signalWriteSettingsSql();
    }
}

void SettingsWidget::closeEvent(QCloseEvent *event)
{
    SettingsWidget::deleteLater();
}

void SettingsWidget::createWidgets()
{
    createTabConnectPbx();
    createTabConnectSql();
    QTabWidget *tabWidget = new QTabWidget(this);
    tabWidget->addTab(tabConnectPbx,
                      QIcon(":/images/telephone_blue.png"), tr("Connect PBX"));
    tabWidget->addTab(tabConnectSql,
                      QIcon(":/images/database_red.png"), tr("Connect SQL"));

    QHBoxLayout *horLayout = new QHBoxLayout;
    horLayout->addWidget(tabWidget);
    setLayout(horLayout);
}

void SettingsWidget::createTabConnectPbx()
{
    tabConnectPbx = new QWidget;
    QValidator *validator = new QIntValidator(1, 100000, this);
    lineEditPortPbx = new QLineEdit;
    lineEditPortPbx->setValidator(validator);
    lineEditPortPbx->setText(QString::number(portPbx));

    QFormLayout *formLayout = new QFormLayout;
    formLayout->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);
    formLayout->addRow(tr("&Listening port:"), lineEditPortPbx);
    tabConnectPbx->setLayout(formLayout);
}

void SettingsWidget::createTabConnectSql()
{
    tabConnectSql = new QWidget;

    lineEditHost = new QLineEdit;
    lineEditHost->setText(hostName);

    lineEditBase = new QLineEdit;
    lineEditBase->setText(baseName);

    lineEditUser = new QLineEdit;
    lineEditUser->setText(userName);

    lineEditPassword = new QLineEdit;
    lineEditPassword->setText(password);

    QFormLayout *formLayout = new QFormLayout;
    formLayout->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);
    formLayout->addRow(tr("&Host name:"), lineEditHost);
    formLayout->addRow(tr("&Base name:"), lineEditBase);
    formLayout->addRow(tr("&User name:"), lineEditUser);
    formLayout->addRow(tr("&Password:"), lineEditPassword);
    tabConnectSql->setLayout(formLayout);
}

void SettingsWidget::readSettings()
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("/connectPBX");
    portPbx = settings.value("/port", 2112).toInt();
    settings.endGroup();

    settings.beginGroup("/connectSQL");
    hostName = settings.value("/host").toString();
    baseName = settings.value("/base").toString();
    userName = settings.value("/user").toString();
    password = settings.value("/password").toString();
    settings.endGroup();
}

void SettingsWidget::writeSettings()
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("/connectPBX");
    settings.setValue("/port", lineEditPortPbx->text().toInt());
    settings.endGroup();

    settings.beginGroup("/connectSQL");
    settings.setValue("/host", lineEditHost->text());
    settings.setValue("/base", lineEditBase->text());
    settings.setValue("/user", lineEditUser->text());
    settings.setValue("/password", lineEditPassword->text());
    settings.endGroup();
}
