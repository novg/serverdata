#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QPlainTextEdit;
class QLabel;
class QCheckBox;
class QSettings;
class QSystemTrayIcon;
class QAction;
class ListenServer;
class ClientSql;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void slotChangeIcon();
    void slotShowHide();
    void slotAbout();
    void slotSettings();
    void slotSetClientName(const QString &name);
    void slotParseText(const QString &txt);
    void slotShowMessages(int state);
    void slotReloadSettingsPbx();
    void slotReloadSettingsSql();

private:
    void readSettings();
    void createWidgets();
    void createAction();
    void createConnect();

private:
    QAction *actionSettings;
    QAction *actionShowHide;
    QAction *actionAbout;
    QAction *actionQuit;
    QPlainTextEdit *textEdit;
    QLabel *labelClient;
    QCheckBox *checkShowMessages;
    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
    QString hostName;
    QString baseName;
    QString userName;
    QString password;
    QString tableName;
    int portPbx;
    bool iconSwitcher;
    bool isShow;
    ListenServer *listenServer;
    ClientSql *clientSql;
};

#endif // MAINWINDOW_H
