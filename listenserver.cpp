#include "listenserver.h"

#include <QSettings>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>

ListenServer::ListenServer(QObject *parent) : QObject(parent)
{
    readSettings();
    tcpServer = new QTcpServer(this);
    createTcpServer(port);
    connect(tcpServer, SIGNAL(newConnection()), SLOT(slotNewConnection()));
}

ListenServer::~ListenServer()
{

}

void ListenServer::slotNewConnection()
{
    QTcpSocket *clientSocket = tcpServer->nextPendingConnection();
    connect(clientSocket, SIGNAL(disconnected()), clientSocket, SLOT(deleteLater()));
    connect(clientSocket, SIGNAL(disconnected()),SLOT(slotShowClient()));
    connect(clientSocket, SIGNAL(readyRead()), SLOT(slotReadClient()));

    sendToClient(clientSocket, "Server Response: Connected!");
    emit signalSendClientName(clientSocket->peerAddress().toString());
    emit signalChangeIcon();
}

void ListenServer::slotReadClient()
{
    // Получаем объект сокета, который вызвал данный слот
    QTcpSocket *clientSocket = (QTcpSocket*)sender();
    QByteArray buff;

    buff.append(clientSocket->readAll());
    QString str = buff.data();

    emit signalSendText(str.toUtf8());
}

void ListenServer::slotShowClient()
{
    emit signalSendClientName("");
    emit signalChangeIcon();
}

void ListenServer::readSettings()
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("/connectPBX");
    port = settings.value("/port", 2112).toInt();
    settings.endGroup();
}

void ListenServer::createTcpServer(int nPort)
{
    if (tcpServer->isListening()) {
        tcpServer->close();
    }

    if (!tcpServer->listen(QHostAddress::Any, nPort)) {
        QMessageBox::critical(0,
                              "Server Error",
                              "Unable to start the Server:"
                              + tcpServer->errorString()
                              );
        tcpServer->close();
        return;
    }
}

void ListenServer::sendToClient(QTcpSocket *pSocket, const QString &str)
{
    QByteArray buff = str.toLocal8Bit();
    pSocket->write(buff);
}
