#-------------------------------------------------
#
# Project created by QtCreator 2015-01-26T14:15:19
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = serverData
TEMPLATE = app

CONFIG += c++14

RC_FILE = icon.rc

SOURCES += main.cpp\
        mainwindow.cpp \
    settingswidget.cpp \
    listenserver.cpp \
    clientsql.cpp

HEADERS  += mainwindow.h \
    settingswidget.h \
    listenserver.h \
    clientsql.h

RESOURCES += \
    images.qrc
