#include "mainwindow.h"
#include "settingswidget.h"
#include "listenserver.h"
#include "clientsql.h"

#include <QPlainTextEdit>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QFile>
#include <QSettings>
#include <QSystemTrayIcon>
#include <QBoxLayout>
#include <QValidator>
#include <QAction>
#include <QApplication>
#include <QMenu>
#include <QMessageBox>
#include <QToolBar>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    iconSwitcher = true;
    isShow       = false;
    listenServer = new ListenServer(this);
    clientSql    = new ClientSql(this);

    createWidgets();
    createAction();
    createConnect();
}

MainWindow::~MainWindow()
{
}

void MainWindow::slotChangeIcon()
{
    iconSwitcher = !iconSwitcher;
    QString strPixmapName = iconSwitcher ? "://images/socket.png"
                                         : "://images/linneighborhood.png";
    trayIcon->setIcon(QPixmap(strPixmapName));
}

void MainWindow::slotShowHide()
{
    setVisible(!isVisible());
}

void MainWindow::slotAbout()
{
    QMessageBox::about(0, "About", "<h1><b><i><u>serverData ver. 0.8</u></i></b></h1>"
                       "produced by: A. Novgorodskiy<br/>"
                       "email: <a href=\"mailto:novg.novg@gmail.com\">novg.novg@gmail.com</a><br/>"
                       );
}

void MainWindow::slotSettings()
{
    SettingsWidget *settingsWidget = new SettingsWidget;
    settingsWidget->show();
    connect(settingsWidget, SIGNAL(signalWriteSettingsPbx()),
            SLOT(slotReloadSettingsPbx()));
    connect(settingsWidget, SIGNAL(signalWriteSettingsSql()),
            SLOT(slotReloadSettingsSql()));
}

void MainWindow::slotSetClientName(const QString &name)
{
    labelClient->setText(tr("Connected with the Client: ") + name);
}

void MainWindow::slotParseText(const QString &txt)
{
    clientSql->fillTable(txt);
    if (isShow) {
        textEdit->moveCursor(QTextCursor::End);
        textEdit->insertPlainText(txt);
    }
}

void MainWindow::slotShowMessages(int state)
{
    if (state != 0) {
        isShow = true;
    }
    else {
        isShow = false;
        textEdit->clear();
    }
}

void MainWindow::slotReloadSettingsPbx()
{
    listenServer->deleteLater();
    listenServer = new ListenServer(this);
    slotChangeIcon();
    slotSetClientName("");
    connect(listenServer, SIGNAL(signalSendClientName(QString)),
            SLOT(slotSetClientName(QString)));
    connect(listenServer, SIGNAL(signalChangeIcon()), SLOT(slotChangeIcon()));
    connect(listenServer, SIGNAL(signalSendText(QString)),
            SLOT(slotParseText(QString)));
}

void MainWindow::slotReloadSettingsSql()
{
    clientSql->deleteLater();
    clientSql = new ClientSql(this);
}

void MainWindow::readSettings()
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("/connectPBX");
    portPbx = settings.value("/port", 2112).toInt();
    settings.endGroup();

    settings.beginGroup("/connectSQL");
    hostName = settings.value("/host").toString();
    baseName = settings.value("/base").toString();
    userName = settings.value("/user").toString();
    password = settings.value("/password").toString();
    settings.endGroup();
}

void MainWindow::createWidgets()
{
    textEdit = new QPlainTextEdit;
    textEdit->setReadOnly(true);

    QFont font;
    font.setFamily("Lucida Console");
    textEdit->setFont(font);

    labelClient = new QLabel(tr("Connected with the Client:"));
    checkShowMessages = new QCheckBox(tr("&Show messages"));

    //Layout setup
    QHBoxLayout *bottomLayout = new QHBoxLayout;
    bottomLayout->addWidget(checkShowMessages);
    bottomLayout->addStretch(1);
    bottomLayout->addWidget(labelClient);

    QVBoxLayout *pvbxLayout = new QVBoxLayout;
    pvbxLayout->addWidget(textEdit);
    pvbxLayout->addLayout(bottomLayout);

    QWidget *widget = new QWidget(this);
    widget->setLayout(pvbxLayout);
    setCentralWidget(widget);
}

void MainWindow::createAction()
{
    actionShowHide = new QAction(tr("&Show/Hide"), this);
    actionShowHide->setIcon(QIcon(":/images/Pictures.png"));

    actionAbout = new QAction(tr("&About"), this);
    actionAbout->setIcon(QIcon(":/images/Info.png"));

    actionQuit = new QAction(tr("&Qiut"), this);
    actionQuit->setIcon(QIcon(":/images/Shutdown.png"));

    actionSettings = new QAction(tr("Settings"), this);
    actionSettings->setIcon(QIcon(":/images/cog-icon-2-48x48.png"));

    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(actionShowHide);
    trayIconMenu->addAction(actionSettings);
    trayIconMenu->addAction(actionAbout);
    trayIconMenu->addAction(actionQuit);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setToolTip(tr("serverData"));

    slotChangeIcon();
    trayIcon->show();
}

void MainWindow::createConnect()
{
    connect(actionShowHide, SIGNAL(triggered()), SLOT(slotShowHide()));
    connect(actionAbout, SIGNAL(triggered()), SLOT(slotAbout()));
    connect(actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
    connect(actionSettings, SIGNAL(triggered()), SLOT(slotSettings()));
    connect(listenServer, SIGNAL(signalSendClientName(QString)),
            SLOT(slotSetClientName(QString)));
    connect(listenServer, SIGNAL(signalChangeIcon()), SLOT(slotChangeIcon()));
    connect(listenServer, SIGNAL(signalSendText(QString)),
            SLOT(slotParseText(QString)));
    connect(checkShowMessages, SIGNAL(stateChanged(int)),
            SLOT(slotShowMessages(int)));
}
