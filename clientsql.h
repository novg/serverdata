#ifndef CLIENTSQL_H
#define CLIENTSQL_H

#include <QObject>

class ClientSql : public QObject
{
    Q_OBJECT
public:
    explicit ClientSql(QObject *parent = 0);
    ~ClientSql();

public:
    void fillTable(const QString &str);
signals:

public slots:

private:
    void readSettings();
    bool createConnection();
    void createTable();
    bool insertValues(const QStringList &listValues);

private:
    QString tableName;
    QString hostName;
    QString baseName;
    QString userName;
    QString password;
};

#endif // CLIENTSQL_H
