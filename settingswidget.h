#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QWidget>

class QSettings;
class QLineEdit;

class SettingsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SettingsWidget(QWidget *parent = 0);
    ~SettingsWidget();

signals:
    void signalWriteSettingsPbx();
    void signalWriteSettingsSql();

public slots:

private:
    void closeEvent(QCloseEvent *event);
    void createWidgets();
    void createTabConnectPbx();
    void createTabConnectSql();
    void readSettings();
    void writeSettings();

private:
    QWidget *tabConnectPbx;
    QWidget *tabConnectSql;
    QLineEdit *lineEditPortPbx;
    QLineEdit *lineEditHost;
    QLineEdit *lineEditBase;
    QLineEdit *lineEditUser;
    QLineEdit *lineEditPassword;
    QString hostName;
    QString baseName;
    QString userName;
    QString password;
    QString tableName;
    int portPbx;
};

#endif // SETTINGSWIDGET_H
