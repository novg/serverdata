#include "clientsql.h"

#include <QSettings>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDate>

ClientSql::ClientSql(QObject *parent) : QObject(parent)
{
    readSettings();
    tableName = "calls";
    createTable();
}

ClientSql::~ClientSql()
{

}

void ClientSql::fillTable(const QString &str)
{

    QString year = QDate::currentDate().toString("yyyy");
    QRegExp rxDateTime;
    rxDateTime.setPattern("(\\d{2})(\\d{2})(\\d{2})(\\d{2})");
    QRegExp rxDuration;
    rxDuration.setPattern("(\\d{1})(\\d{2})(\\d{2})");

    QString line = str;
    line.replace("\n", "");
    QStringList lst = line.split(" ", QString::SkipEmptyParts);

    if (lst.size() < 6) {
        return;
    }
    if (rxDateTime.indexIn(lst[0]) != -1) {
        lst[0] = year + "-" + rxDateTime.cap(1) + "-" + rxDateTime.cap(2)
                 + " " + rxDateTime.cap(3) + ":" + rxDateTime.cap(4);
    }
    if (rxDuration.indexIn(lst[1]) != -1) {
        lst[1] = "0" + rxDuration.cap(1) + ":" + rxDuration.cap(2) + ":"
                 + rxDuration.cap(3);
    }
    if (!lst[2].contains(QRegExp("[A-Za-z]"))) {
        lst.insert(2, "");
    }
    if(lst.size() != 9) {
        lst.insert(3, "NULL");
    }
    if(lst.size() != 9) {
        lst.insert(4, "");
    }
    if (!insertValues(lst)) {
        return;
    }
}

void ClientSql::readSettings()
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("/connectSQL");
    hostName = settings.value("/host").toString();
    baseName = settings.value("/base").toString();
    userName = settings.value("/user").toString();
    password = settings.value("/password").toString();
    settings.endGroup();

}

bool ClientSql::createConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName(hostName);
    db.setDatabaseName(baseName);
    db.setUserName(userName);
    db.setPassword(password);

    if (!db.open()) {
        QMessageBox::critical(0,
                              "SQL Server Error",
                              "Cannot open database:"
                              + db.lastError().text()
                              );
        return false;
    }

    return true;
}

void ClientSql::createTable()
{
    if (!createConnection()) {
        return;
    }

    QString sqlCreateTable = QString(
                "CREATE TABLE IF NOT EXISTS %1 ( "
                "datetime TIMESTAMP, "
                "duration INTERVAL, "
                "seg      VARCHAR, "
                "sop      VARCHAR, "
                "dest     VARCHAR, "
                "numin    VARCHAR, "
                "numout   VARCHAR, "
                "str1     VARCHAR, "
                "str2     VARCHAR "
                ");").arg(tableName);

    QSqlQuery query;
    if (!query.exec(sqlCreateTable)) {
        QMessageBox::critical(0,
                              "SQL Server Error",
                              "Unable to create a table:\n" + tableName
                              );
        return;
    }
}

bool ClientSql::insertValues(const QStringList &listValues)
{
    QString sqlInsert = QString(
                "INSERT INTO %1 (datetime, duration, seg, sop, "
                "dest, numin, numout, str1, str2) "
                "VALUES('%2', '%3', '%4', %5, '%6', '%7', '%8', '%9', %10)"
                ).arg(tableName);

    QString str = sqlInsert;
    foreach (QString var, listValues) {
        str = str.arg(var);
    }

    QSqlQuery query;
    if (!query.exec(str)) {
        QMessageBox::critical(0,
                              "SQL Server Error",
                              "Unable to make insert operation:\n" + str
                              );
        return false;
    }

    return true;
}

