#ifndef LISTENSERVER_H
#define LISTENSERVER_H

#include <QObject>

class QTcpServer;
class QTcpSocket;

class ListenServer : public QObject
{
    Q_OBJECT
public:
    explicit ListenServer(QObject *parent = 0);
    ~ListenServer();

signals:
    void signalSendText(const QString &txt);
    void signalSendClientName(const QString &name);
    void signalChangeIcon();

public slots:

private slots:
    void slotNewConnection();
    void slotReadClient();
    void slotShowClient();
//    void slotChangeIcon();

private:
    void readSettings();
    void createTcpServer(int nPort);
    void sendToClient(QTcpSocket *pSocket, const QString &str);

private:
    int port;
    QTcpServer *tcpServer;
};

#endif // LISTENSERVER_H
