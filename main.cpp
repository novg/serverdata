#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
//    w.show();
    w.resize(600, 300);
    w.setWindowIcon(QIcon("://images/socket.png"));
    QApplication::setQuitOnLastWindowClosed(false);

    return a.exec();
}
